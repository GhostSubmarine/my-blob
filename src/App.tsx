import InitRouter from 'router/initRouter'
import { StrictMode } from 'react'

const App = () => 
  <StrictMode>
    <InitRouter />
  </StrictMode>

export default App
