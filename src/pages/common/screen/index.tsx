import { useEffect, useState } from 'react'
import { NavLink, useLocation } from 'react-router-dom';
import Vjkzw from 'assets/images/v-jkzw.png'
import Vmdcs from 'assets/images/v-mdcs.png'
import Logojkzw from 'assets/images/logo-jkzw.png'
import Logomdcs from 'assets/images/logo-mdcs.png'

const SVGObj: { [key: string]: string; } = {
  dispatcher: 'icon-tiaodutai',
  workShiftManagement: 'icon-paibanguanli',
  maintenanceScheduling: 'icon-weihujihua',
  workOrderManagement: 'icon-guichengguanli',
  systemSettings: 'icon-xitongshezhi',
  infoManage: 'icon-tiaodutai',
  procedureManagement: 'icon-guichengguanli',
  emergencyResponse: 'icon-yingjichuzhi',
  statisticalReport: 'icon-tongjibaobiao',
  tIManage: 'icon-tiaodutai',
}

interface MenuObj {
  address?: string;
  applicationId: number;
  applicationIndex: number;
  children: Array<MenuObj>;
  label: string;
  name: string;
  note: string;
  parentId: string;
  permissionId: string;
  permissionType: string;
}

const str = '{"permissionId":"7","applicationId":7,"permissionType":"0","parentId":"0","label":"维修调度","name":"MDCS","note":null,"address":"/mdcs/mdcs-web/#/","applicationIndex":7000000,"children":[{"permissionId":"7.1","applicationId":7,"permissionType":"1","parentId":"7","label":"调度台","name":"dispatcher","note":null,"address":null,"applicationIndex":7100000,"children":null},{"permissionId":"7.2","applicationId":7,"permissionType":"1","parentId":"7","label":"工班管理","name":"workShiftManagement","note":null,"address":null,"applicationIndex":7200000,"children":[{"permissionId":"7.2.1","applicationId":7,"permissionType":"2","parentId":"7.2","label":"工班列表","name":"workShiftList","note":null,"address":null,"applicationIndex":7210000,"children":null},{"permissionId":"7.2.2","applicationId":7,"permissionType":"2","parentId":"7.2","label":"排班管理","name":"shiftManagement","note":null,"address":null,"applicationIndex":7220000,"children":[{"permissionId":"7.2.2.1","applicationId":7,"permissionType":"3","parentId":"7.2.2","label":"维修排班查询","name":"personArrangeQuery","note":null,"address":null,"applicationIndex":7221000,"children":null},{"permissionId":"7.2.2.2","applicationId":7,"permissionType":"3","parentId":"7.2.2","label":"工班排班","name":"teamArrange","note":null,"address":null,"applicationIndex":7222000,"children":null},{"permissionId":"7.2.2.3","applicationId":7,"permissionType":"3","parentId":"7.2.2","label":"调度排班查询","name":"dispatcherArrangeQuery","note":null,"address":null,"applicationIndex":7223000,"children":null}]}]},{"permissionId":"7.3","applicationId":7,"permissionType":"1","parentId":"7","label":"规程管理","name":"procedureManagement","note":null,"address":null,"applicationIndex":7300000,"children":[{"permissionId":"7.3.1","applicationId":7,"permissionType":"2","parentId":"7.3","label":"维修计划规程","name":"maintenanceInspectionProcedures","note":null,"address":null,"applicationIndex":7310000,"children":null},{"permissionId":"7.3.2","applicationId":7,"permissionType":"2","parentId":"7.3","label":"故障维修指引","name":"failureMaintenanceProcedures","note":null,"address":null,"applicationIndex":7320000,"children":null}]},{"permissionId":"7.4","applicationId":7,"permissionType":"1","parentId":"7","label":"维护计划","name":"maintenanceScheduling","note":null,"address":null,"applicationIndex":7400000,"children":[{"permissionId":"7.4.1","applicationId":7,"permissionType":"2","parentId":"7.4","label":"年度检修计划","name":"annualMaintenancePlan","note":null,"address":null,"applicationIndex":7410000,"children":null},{"permissionId":"7.4.2","applicationId":7,"permissionType":"2","parentId":"7.4","label":"月度检修计划","name":"monthlyMaintenancePlan","note":null,"address":null,"applicationIndex":7420000,"children":null},{"permissionId":"7.4.3","applicationId":7,"permissionType":"2","parentId":"7.4","label":"检修周期设置","name":"maintenancePlanManagement","note":null,"address":null,"applicationIndex":7430000,"children":null}]},{"permissionId":"7.5","applicationId":7,"permissionType":"1","parentId":"7","label":"工单管理","name":"workOrderManagement","note":null,"address":null,"applicationIndex":7500000,"children":[{"permissionId":"7.5.1","applicationId":7,"permissionType":"2","parentId":"7.5","label":"工单情况概览","name":"workOrderOverview","note":null,"address":null,"applicationIndex":7510000,"children":null},{"permissionId":"7.5.2","applicationId":7,"permissionType":"2","parentId":"7.5","label":"审批流程配置","name":"appropMenuObjProcessConfig","note":null,"address":null,"applicationIndex":7520000,"children":null}]},{"permissionId":"7.6","applicationId":7,"permissionType":"1","parentId":"7","label":"应急配置","name":"emergencyResponse","note":null,"address":null,"applicationIndex":7600000,"children":[{"permissionId":"7.6.1","applicationId":7,"permissionType":"2","parentId":"7.6","label":"应急事件管理","name":"emergencyManagement","note":null,"address":null,"applicationIndex":7610000,"children":null},{"permissionId":"7.6.2","applicationId":7,"permissionType":"2","parentId":"7.6","label":"类型等级管理","name":"typeLevelManagement","note":null,"address":null,"applicationIndex":7620000,"children":null},{"permissionId":"7.6.3","applicationId":7,"permissionType":"2","parentId":"7.6","label":"应急预案","name":"emergencyPlanLibrary","note":null,"address":null,"applicationIndex":7630000,"children":null},{"permissionId":"7.6.4","applicationId":7,"permissionType":"2","parentId":"7.6","label":"处置案例","name":"caseQuery","note":null,"address":null,"applicationIndex":7640000,"children":null}]},{"permissionId":"7.7","applicationId":7,"permissionType":"1","parentId":"7","label":"系统设置","name":"systemSettings","note":null,"address":null,"applicationIndex":7700000,"children":[{"permissionId":"7.7.1","applicationId":7,"permissionType":"2","parentId":"7.7","label":"线路管理","name":"lineManager","note":null,"address":null,"applicationIndex":7710000,"children":null},{"permissionId":"7.7.2","applicationId":7,"permissionType":"2","parentId":"7.7","label":"专业管理","name":"majorManager","note":null,"address":null,"applicationIndex":7720000,"children":null},{"permissionId":"7.7.3","applicationId":7,"permissionType":"2","parentId":"7.7","label":"设备管理","name":"equipmentManager","note":null,"address":null,"applicationIndex":7730000,"children":null},{"permissionId":"7.7.4","applicationId":7,"permissionType":"2","parentId":"7.7","label":"位置管理","name":"locationManager","note":null,"address":null,"applicationIndex":7740000,"children":null},{"permissionId":"7.7.5","applicationId":7,"permissionType":"2","parentId":"7.7","label":"用户管理","name":"userManager","note":null,"address":null,"applicationIndex":7750000,"children":null},{"permissionId":"7.7.6","applicationId":7,"permissionType":"2","parentId":"7.7","label":"岗位管理","name":"positionManager","note":null,"address":null,"applicationIndex":7760000,"children":null},{"permissionId":"7.7.7","applicationId":7,"permissionType":"2","parentId":"7.7","label":"供应商管理","name":"supplyManager","note":null,"address":null,"applicationIndex":7770000,"children":null},{"permissionId":"7.7.8","applicationId":7,"permissionType":"2","parentId":"7.7","label":"通讯录管理","name":"addressManager","note":null,"address":null,"applicationIndex":7780000,"children":null},{"permissionId":"7.7.9","applicationId":7,"permissionType":"2","parentId":"7.7","label":"应急组织架构管理","name":"organizationStructureManagement","note":null,"address":null,"applicationIndex":7790000,"children":null},{"permissionId":"7.7.11","applicationId":7,"permissionType":"2","parentId":"7.7","label":"操作日志","name":"actionLog","note":null,"address":null,"applicationIndex":7810000,"children":null}]},{"permissionId":"7.10","applicationId":7,"permissionType":"2","parentId":"7","label":"统计报表","name":"statisticalReport","note":"","address":"","applicationIndex":71000000,"children":[{"permissionId":"7.10.1","applicationId":7,"permissionType":"2","parentId":"7.10","label":"报表统计","name":"reportCount","note":"","address":"","applicationIndex":710100000,"children":null},{"permissionId":"7.10.2","applicationId":7,"permissionType":"2","parentId":"7.10","label":"报表模板","name":"reportTemplate","note":null,"address":null,"applicationIndex":710200000,"children":null}]}]}'

const arr = JSON.parse(str) as MenuObj

console.log(arr)

export default function Example() {
  const [expandId, setExpandId] = useState('')
  const [selectId, setSelectId] = useState(arr.children[0].permissionId)
  const [activeId, setActiveId] = useState('')
  const [isFold, setIsFold] = useState(false)
  const path = useLocation()
  // const { menuData, configStatus } = useAuth()
  // const arr = menuData?.find(val => val.name === 'MDCS')?.children
  // const { pathName } = path
  // useEffect(() => {
  //   console.log(activeId)
  // }, [activeId])
  // useEffect(() => {
  //   if (configStatus) {
  //     const arr = pathname.split('/')
  //     arr.shift()
  //     arr.shift()
  //     arr?.forEach(menuObj => {
  //       if (arr.includes(menuObj.name)) {
  //         if (menuObj.children && menuObj.children.length > 0) {
  //           menuObj.children.forEach(cObj => {
  //             if (arr.includes(cObj.name)) {
  //               setExpandId(menuObj.permissionId)
  //               setSelectId(cObj.permissionId)
  //             }
  //           })
  //         }
  //         else {
  //           setSelectId(menuObj.permissionId)
  //         }
  //       }
  //     })
  //   }
  //   else {
  //     setSelectId('7.7')
  //   }
  // }, [pathname])
  // useEffect(() => {
  //   if (isFold) {
  //     setExpandId('')
  //   }
  // }, [isFold])
  return (
    <div className={'w-full h-full'}>
      <header className={'min-h-[66px] bg-blue1 flex items-center justify-between text-white'}>
        <div className={'flex items-center h-[66px]'}>
          <img src={Logojkzw} className={'h-[80px]'} />
          <img src={Logomdcs} className={'h-[66px]'} />
        </div>
        <span className={'text-2xl'}>
          深圳地铁智能运营平台
        </span>
        <div>1</div>
      </header>
      <div className={'flex'} style={{
        height: 'calc(100% - 66px)'
      }}>
        <nav className={'flex flex-col text-white'}
          style={{
            backgroundColor: '#222D3E',
            overflow: 'auto'
          }}>
          <div className={'flex'} style={{
            minHeight: 96
          }}>
            <div className={'text-center'} style={{
              width: 50,
              backgroundColor: '#131B29',
              paddingTop: 50,
              color: '#63738B'
            }}>
              <i className={`iconfont ${isFold ? 'icon-zhankai' : 'icon-shouqi'} cursor-pointer`}
                onClick={() => setIsFold(origin => !origin)} />
            </div>
            <div className={`${isFold ? 'hidden' : ''}`} />
          </div>
          <div className={'flex flex-1 overflow-auto'}>
            <div style={{
              width: 50,
              backgroundColor: '#131B29'
            }}>
              {
                arr.children?.map(pMenuObj => {
                  if (pMenuObj.children && pMenuObj.children.length > 0) {
                    return (
                      <div key={pMenuObj.permissionId}
                        style={{
                          backgroundColor: '#131B29'
                        }}>
                        <div className={`text-center cursor-pointer`}
                          onMouseOver={() => setActiveId(pMenuObj.permissionId)}
                          onMouseOut={() => setActiveId('')}
                          onClick={() => {
                            if (expandId === pMenuObj.permissionId) {
                              setExpandId('')
                            }
                            else {
                              setExpandId(pMenuObj.permissionId)
                            }
                          }}
                          style={{
                            height: 50,
                            lineHeight: '50px',
                            color: pMenuObj.permissionId === activeId || pMenuObj.permissionId === expandId ? '#2E78FF' : '#FFFFFF'
                          }}>
                          <i className={`iconfont ${SVGObj[pMenuObj.name]}`} />
                        </div>
                        <div className={`${expandId === pMenuObj.permissionId ? '' : 'hidden'}`}>
                          {
                            pMenuObj.children.map(cMenuObj =>
                              <div className={'h-10'} key={cMenuObj.permissionId}
                                onClick={() => {
                                  setSelectId(cMenuObj.permissionId)
                                }} />
                            )
                          }
                        </div>
                      </div>
                    )
                  }
                  else {
                    return (
                      <div className={`text-center cursor-pointer`} style={{
                        height: 50,
                        lineHeight: '50px',
                        color: pMenuObj.permissionId === activeId || pMenuObj.permissionId === expandId ? '#2E78FF' : '#FFFFFF'
                      }} onClick={() => {
                        setSelectId(pMenuObj.permissionId)
                      }} onMouseOver={() => setActiveId(pMenuObj.permissionId)}
                        onMouseOut={() => setActiveId('')}>
                        <i className={`iconfont ${SVGObj[pMenuObj.name]}`} />
                      </div>
                    )
                  }
                })
              }
            </div>
            <div className={`${isFold ? 'hidden' : ''} font-semibold flex flex-col`}
              style={{
                minWidth: 110
              }}>
              {
                arr.children?.map(pMenuObj => {
                  if (pMenuObj.children && pMenuObj.children.length > 0) {
                    return (
                      <div key={pMenuObj.permissionId}>
                        <div className={`cursor-pointer pl-6 pr-6`}
                          onMouseOver={() => setActiveId(pMenuObj.permissionId)}
                          onMouseOut={() => setActiveId('')}
                          onClick={() => {
                            if (expandId === pMenuObj.permissionId) {
                              setExpandId('')
                            }
                            else {
                              setExpandId(pMenuObj.permissionId)
                            }
                          }}
                          style={{
                            height: 50,
                            lineHeight: '50px',
                            color: expandId === pMenuObj.permissionId || activeId === pMenuObj.permissionId ? '#FFFFFF' : '#B4C2D5',
                            backgroundColor: expandId === pMenuObj.permissionId || activeId === pMenuObj.permissionId ? '#131B29' : '#222D3E'
                          }}>
                          {pMenuObj.label}
                        </div>
                        <div className={`${expandId === pMenuObj.permissionId ? '' : 'hidden'}`}>
                          {
                            pMenuObj.children.map(cMenuObj =>
                              <div className={'h-10 pl-6 pr-6'} key={cMenuObj.permissionId}
                                onMouseOver={() => setActiveId(cMenuObj.permissionId)}
                                onMouseOut={() => setActiveId('')}
                                style={{
                                  color: selectId === cMenuObj.permissionId || activeId === cMenuObj.permissionId ? '#FFFFFF' : '#B4C2D5',
                                  fontSize: 14,
                                  lineHeight: '40px',
                                  backgroundColor: selectId === cMenuObj.permissionId || activeId === cMenuObj.permissionId ? '#131B29' : '#303D52'
                                }}
                                onClick={() => {
                                  setSelectId(cMenuObj.permissionId)
                                }}>
                                <NavLink to={`main/${pMenuObj.name}/${cMenuObj.name}`}>
                                  {cMenuObj.label}
                                </NavLink>
                              </div>
                            )
                          }
                        </div>
                      </div>
                    )
                  }
                  else {
                    return (
                      <div className={`cursor-pointer pl-6 pr-6`} style={{
                        height: 50,
                        lineHeight: '50px',
                        backgroundColor: selectId === pMenuObj.permissionId || activeId === pMenuObj.permissionId ? '#131B29' : '#222D3E',
                        color: selectId === pMenuObj.permissionId || activeId === pMenuObj.permissionId ? '#FFFFFF' : '#B4C2D5',
                      }} onClick={() => {
                        setSelectId(pMenuObj.permissionId)
                      }} onMouseOver={() => setActiveId(pMenuObj.permissionId)}
                        onMouseOut={() => setActiveId('')}>
                        <NavLink to={`main/${pMenuObj.name}`}>
                          {pMenuObj.label}
                        </NavLink>
                      </div>
                    )
                  }
                })
              }
            </div>
          </div>
          <div className={'flex text-center text-sm mt-auto'}
            style={{
              minHeight: 160
            }}>
            <div style={{
              width: 50,
              backgroundColor: '#131B29'
            }} />
            <div className={`flex flex-col justify-end gap-4 items-center flex-1 pb-5 ${isFold ? 'hidden' : ''}`}>
              <img src={Vmdcs} className={'h-7'} />
              <img src={Vjkzw} className={'h-5'} />
              <span className={'text-sm'}
                style={{
                  color: '#63738B'
                }}>MDCS-V1.6.1.0</span>
            </div>
          </div>
        </nav>
        <main className={'flex-1 p-[14px] pb-[23px] bg-lightGrey'}>
        <ul></ul>
        
        </main>
      </div>
    </div>
  )
}