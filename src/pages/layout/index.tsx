const MainPage = () => {
  // const [text, setText] = useState('')
//   const text = `q paragragh with *emphasis* and **strong importance**
// > A block quote with ~strikethrough~ and a URL: https://reactjs.org.

// * Lists
// * [ ] todo
// * [x] done
// A table:

// | a | b |
// | - | - |
//   `
  // useMount(async () => {
  //   const res = await fetch('./doc/hello.md')
  //   const content = await res.text()
  //   setText(content)
  // })
  return (
    <div className={'md:w-[640px] sm:w-full h-[640px] mx-auto bg-gray-400 text-white p-8 h-full flex flex-col'}>
      <div className={'mx-auto inline-flex flex-col gap-10 mt-10'}>
        <button className="py-2 px-4 font-semibold rounded-lg shadow-md text-white bg-green-500 hover:bg-green-700">开始游戏</button>
        <button className="py-2 px-4 font-semibold rounded-lg shadow-md text-white bg-green-500 hover:bg-green-700">游戏设置</button>
        <button className="py-2 px-4 font-semibold rounded-lg shadow-md text-white bg-green-500 hover:bg-green-700">制作名单</button>
        <button className="py-2 px-4 font-semibold rounded-lg shadow-md text-white bg-green-500 hover:bg-green-700">退出游戏</button>
      </div>
      <div className={'mt-40 mx:auto'}>
        34
      </div>
    </div>
  )
}

export default MainPage