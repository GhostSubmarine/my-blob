import LoginPage from 'pages/common/login'
import ScreenPage from 'pages/common/screen'
import MainPage from 'pages/layout'
import { HashRouter, Route, Routes } from 'react-router-dom'


const InitRouter = () => 
  <HashRouter>
    <Routes>
      <Route path={'/'} element={<MainPage />} />
      <Route path={'/login'} element={<LoginPage />} />
      <Route path={'/screen'} element={<ScreenPage />} />
    </Routes>
  </HashRouter>

export default InitRouter 