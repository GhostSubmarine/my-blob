module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './templatePublic/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        sm: '640px',
        md: '768px',
        lg: '1024px',
        xl: '1280px',
        '2xl': '1920px'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}