const path = require('path')

module.exports = {
  webpack: {
    configure: (webpackConfig, { env, paths }) => {
      // webpackConfig.plugins[0].userOptions.template = './templatePublic/index.html'
      // webpackConfig.output.path = path.resolve(__dirname, 'public')
      return webpackConfig
    }
  },
  style: {
    postcssOptions: {
      plugins: [
        require('tailwindcss'), 
        require('autoprefixer')
      ],
    },
  },
  rules: [
    {
      test: /\.md$/,
      loader: 'raw-loader'
    }
  ]
}